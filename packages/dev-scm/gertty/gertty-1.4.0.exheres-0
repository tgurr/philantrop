# Copyright 2015 Thomas G. Anderson
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools blacklist="3" ]

SUMMARY="A console-based interface to the Gerrit Code Review system"
DESCRIPTION="Advantages compared to the Gerrit web interface:
* Workflow – the interface is designed to support a workflow similar to reading
  network news or mail. In particular, it is designed to deal with a large number
  of review requests across a large number of projects.
* Offline Use – Gertty syncs information about changes in subscribed projects
  to a local database and local git repos. All review operations are performed
  against that database and then synced back to Gerrit.
* Speed – user actions modify locally cached content and need not wait for
  server interaction.
* Convenience – because Gertty downloads all changes to local git repos, a
  single command instructs it to checkout a change into that repo for detailed
  examination or testing of larger changes.
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/alembic[>=0.6.4][python_abis:*(-)?]
        dev-python/GitPython[>=0.3.2_rc1][python_abis:*(-)?]
        dev-python/pbr[>=0.11.0&<2.0][python_abis:*(-)?]
        dev-python/ply[>=3.4][python_abis:*(-)?]
        dev-python/python-dateutil[python_abis:*(-)?]
        dev-python/PyYAML[>=3.1.0][python_abis:*(-)?]
        dev-python/requests[>=2.5.3&<3.0.0][python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        dev-python/SQLAlchemy[>=1.0.4][python_abis:*(-)?]
        dev-python/urwid[>=1.2.1][python_abis:*(-)?]
        dev-python/voluptuous[>=0.7][python_abis:*(-)?]
    test:
        dev-python/ordereddict[python_abis:*(-)?]
        dev-python/urwid[>=1.2.1&<1.3.0][python_abis:*(-)?]
"

BUGS_TO="tanderson@caltech.edu philantrop@exherbo.org"

# Has no tests but the urwid dependency is annoying so we restrict the tests here.
# Last checked: 1.4.0
RESTRICT="test"

src_install() {
    setup-py_src_install

    insinto /usr/share/${PN}/examples

    hereins exherbo-${PN}.yaml <<EOF
# This is an example ~/.gertty.yaml file for use with the exherbo gerrit instance.

servers:
  - name: Exherbo
    url: https://galileo.mailstation.de/gerrit/
    auth-type: basic
    # Ask Philantrop or zlin to generate an HTTP password for you.
    # Go to Gerrit -> Settings -> HTTP Password to see your username and password
    username: <your_user_name>
    password: <http_password>
    git-root: ~/path/to/your/repositories/
EOF

    elog "
If you want to use ${PN} with Exherbo's Gerrit instance, an example
file has been installed to /usr/share/${PN}/examples/exherbo-${PN}.yaml"
}

